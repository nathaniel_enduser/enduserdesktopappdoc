var annotated_dup =
[
    [ "Ui", "namespace_ui.html", "namespace_ui" ],
    [ "CReportDialog", "class_c_report_dialog.html", "class_c_report_dialog" ],
    [ "Device", "struct_device.html", "struct_device" ],
    [ "lockunlockDialog", "classlockunlock_dialog.html", "classlockunlock_dialog" ],
    [ "MainWindow", "class_main_window.html", "class_main_window" ],
    [ "Namespace", "struct_namespace.html", "struct_namespace" ],
    [ "passwordDialog", "classpassword_dialog.html", "classpassword_dialog" ],
    [ "Ui_Dialog", "class_ui___dialog.html", "class_ui___dialog" ],
    [ "Ui_lockunlockDialog", "class_ui__lockunlock_dialog.html", "class_ui__lockunlock_dialog" ],
    [ "Ui_MainWindow", "class_ui___main_window.html", "class_ui___main_window" ],
    [ "Ui_passwordDialog", "class_ui__password_dialog.html", "class_ui__password_dialog" ]
];