var files_dup =
[
    [ "creportdialog.h", "creportdialog_8h.html", [
      [ "CReportDialog", "class_c_report_dialog.html", "class_c_report_dialog" ]
    ] ],
    [ "lockunlockdialog.cpp", "lockunlockdialog_8cpp.html", null ],
    [ "lockunlockdialog.h", "lockunlockdialog_8h.html", "lockunlockdialog_8h" ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "mainwindow.cpp", "mainwindow_8cpp.html", null ],
    [ "mainwindow.h", "mainwindow_8h.html", "mainwindow_8h" ],
    [ "passworddialog.cpp", "passworddialog_8cpp.html", null ],
    [ "passworddialog.h", "passworddialog_8h.html", [
      [ "passwordDialog", "classpassword_dialog.html", "classpassword_dialog" ]
    ] ],
    [ "ui_EncDecKeysdialog.h", "ui___enc_dec_keysdialog_8h.html", [
      [ "Ui_Dialog", "class_ui___dialog.html", "class_ui___dialog" ],
      [ "Dialog", "class_ui_1_1_dialog.html", null ]
    ] ],
    [ "ui_Eventsdialog.h", "ui___eventsdialog_8h.html", [
      [ "Ui_Dialog", "class_ui___dialog.html", "class_ui___dialog" ],
      [ "Dialog", "class_ui_1_1_dialog.html", null ]
    ] ],
    [ "ui_HiddenVolumedialog.h", "ui___hidden_volumedialog_8h.html", [
      [ "Ui_Dialog", "class_ui___dialog.html", "class_ui___dialog" ],
      [ "Dialog", "class_ui_1_1_dialog.html", null ]
    ] ],
    [ "ui_lockunlockdialog.h", "ui__lockunlockdialog_8h.html", [
      [ "Ui_lockunlockDialog", "class_ui__lockunlock_dialog.html", "class_ui__lockunlock_dialog" ],
      [ "lockunlockDialog", "class_ui_1_1lockunlock_dialog.html", null ]
    ] ],
    [ "ui_mainwindow.h", "ui__mainwindow_8h.html", [
      [ "Ui_MainWindow", "class_ui___main_window.html", "class_ui___main_window" ],
      [ "MainWindow", "class_ui_1_1_main_window.html", null ]
    ] ],
    [ "ui_Passworddialog.h", "ui___passworddialog_8h.html", [
      [ "Ui_passwordDialog", "class_ui__password_dialog.html", "class_ui__password_dialog" ],
      [ "passwordDialog", "class_ui_1_1password_dialog.html", null ]
    ] ],
    [ "ui_ProtectZonedialog.h", "ui___protect_zonedialog_8h.html", [
      [ "Ui_Dialog", "class_ui___dialog.html", "class_ui___dialog" ],
      [ "Dialog", "class_ui_1_1_dialog.html", null ]
    ] ],
    [ "ui_SecureErasedialog.h", "ui___secure_erasedialog_8h.html", [
      [ "Ui_Dialog", "class_ui___dialog.html", "class_ui___dialog" ],
      [ "Dialog", "class_ui_1_1_dialog.html", null ]
    ] ],
    [ "ui_Settingsdialog.h", "ui___settingsdialog_8h.html", [
      [ "Ui_Dialog", "class_ui___dialog.html", "class_ui___dialog" ],
      [ "Dialog", "class_ui_1_1_dialog.html", null ]
    ] ],
    [ "ui_SignatureUpdatedialog.h", "ui___signature_updatedialog_8h.html", [
      [ "Ui_Dialog", "class_ui___dialog.html", "class_ui___dialog" ],
      [ "Dialog", "class_ui_1_1_dialog.html", null ]
    ] ]
];