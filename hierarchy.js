var hierarchy =
[
    [ "Device", "struct_device.html", null ],
    [ "Namespace", "struct_namespace.html", null ],
    [ "QDialog", null, [
      [ "CReportDialog", "class_c_report_dialog.html", null ],
      [ "lockunlockDialog", "classlockunlock_dialog.html", null ],
      [ "passwordDialog", "classpassword_dialog.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "Ui_Dialog", "class_ui___dialog.html", [
      [ "Ui::Dialog", "class_ui_1_1_dialog.html", null ]
    ] ],
    [ "Ui_lockunlockDialog", "class_ui__lockunlock_dialog.html", [
      [ "Ui::lockunlockDialog", "class_ui_1_1lockunlock_dialog.html", null ]
    ] ],
    [ "Ui_MainWindow", "class_ui___main_window.html", [
      [ "Ui::MainWindow", "class_ui_1_1_main_window.html", null ]
    ] ],
    [ "Ui_passwordDialog", "class_ui__password_dialog.html", [
      [ "Ui::passwordDialog", "class_ui_1_1password_dialog.html", null ]
    ] ]
];