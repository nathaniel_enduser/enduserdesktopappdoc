var mainwindow_8h =
[
    [ "Namespace", "struct_namespace.html", "struct_namespace" ],
    [ "Device", "struct_device.html", "struct_device" ],
    [ "MainWindow", "class_main_window.html", "class_main_window" ],
    [ "DEVICE_COUNT", "mainwindow_8h.html#a268c0726d6884c56c5c4d0b96ac162cd", null ],
    [ "NAMESPACE_COUNT", "mainwindow_8h.html#a2fd1440048958de85fd84b2b15996d63", null ],
    [ "REPORT_CONTENTS", "mainwindow_8h.html#a6dd72eda0a41709952d9b5d0c0e8f9c1", null ],
    [ "REPORT_HEADER", "mainwindow_8h.html#a0fdb42c9fc03214e7433b8695c64c277", null ],
    [ "DEVICE_DATA", "mainwindow_8h.html#acfbbf45a894b3af90748a94ce205003a", null ],
    [ "NAMESPACE_DATA", "mainwindow_8h.html#a220292cad95a0795feac8cb08ea22922", null ]
];